package com.example.studentmanagement

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.studentmanagement.database.StudentDatabaseHelper
import com.example.studentmanagement.databinding.ActivityAddStudentDetailBinding
import com.example.studentmanagement.model.StudentModel


class AddStudentDetail : AppCompatActivity(),
    AdapterView.OnItemSelectedListener {
    private lateinit var binding: ActivityAddStudentDetailBinding


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_student_detail)

        binding = ActivityAddStudentDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //initView()
        val edName = binding.edtName
        val edPhone = binding.enterPhone
        val edIdStudent = binding.enterMsv
        var gender: String = ""
        val edAddress = binding.enterAddress
        val edSemesterGrade = binding.txtDiemkh1
        val edSemesterGrade2 = binding.txtDiemhk2
        val informationOther = binding.differentinfomationAddviewstd

        val btnAdd = binding.btnAdd
        val btnDelete = binding.btnDelete

        val genderRadioGroup: RadioGroup = binding.genderRadiobtn
        val maleRadioButton: RadioButton = binding.maleRadiobtn
        val femaleRadioButton: RadioButton = binding.femaleRadiobrn


        var finalGrade = 0
        var isEditMode: Boolean
        //------------------
        val spinnerYear: Spinner = binding.spinneryear
        val years = mutableListOf<String>()
        for (year in 1990..2015) {
            years.add(year.toString())
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, years)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerYear.adapter = adapter
        spinnerYear.onItemSelectedListener = this
        //------------------
        genderRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.male_radiobtn -> {
                    // Xử lý khi người dùng chọn giới tính Nam
                    gender = "Nam"
                }

                R.id.female_radiobrn -> {
                    // Xử lý khi người dùng chọn giới tính Nữ
                    gender = "Nữ"
                }
            }
        }

        val dbHandler = StudentDatabaseHelper(this)
        if (intent != null && intent.getStringExtra("Mode") == "E") {
            //update data
            isEditMode = true
            btnAdd.text = "Update Data"
            btnDelete.visibility = View.VISIBLE
            val students: StudentModel? = dbHandler.getStudent(intent.getIntExtra("Id", 0))
            if (students != null) {
                edName.setText(students.name)
            }
            if (students != null) {
                edPhone.setText(students.phone)
                edIdStudent.setText(students.idStudent)
                edAddress.setText(students.address)
                edSemesterGrade.setText(students.semesterGrade1.toString())
                edSemesterGrade2.setText(students.semesterGrade2.toString())
                informationOther.setText(students.informationOther)
                val birthYearIndex = years.indexOf(students.birthDay)
                spinnerYear.setSelection(birthYearIndex)
                // Lấy giá trị giới tính từ students.gender (đã được cập nhật trong RadioGroup)
                if (students.gender == "Nam") {
                    maleRadioButton.isChecked = true
                } else if (students.gender == "Nữ") {
                    femaleRadioButton.isChecked = true
                }
            }

        } else {
            //insert new data
            isEditMode = false
            btnAdd.text = "Thêm"
            btnDelete.visibility = View.GONE
        }
        btnAdd.setOnClickListener {

            if (isEditMode) {
                val students = StudentModel()
                students.id = intent.getIntExtra("Id", 0)
                students.name = edName.text.toString()
                students.phone = edPhone.text.toString()
                val year = spinnerYear.selectedItem.toString()
                students.birthDay = year
                students.idStudent = edIdStudent.text.toString()
                students.gender = gender
                students.address = edAddress.text.toString()
                students.semesterGrade1 = edSemesterGrade.text.toString().toDouble()
                students.semesterGrade2 = edSemesterGrade2.text.toString().toDouble()
                students.informationOther = informationOther.text.toString()

                // Gọi hàm updateStudent và kiểm tra kết quả
                val dbHandler = StudentDatabaseHelper(this)
                val success = dbHandler.updateStudent(students)
                if (success) {
                    Toast.makeText(this, "Đã cập nhật dữ liệu học sinh", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this, "Có lỗi xảy ra khi cập nhật dữ liệu", Toast.LENGTH_SHORT)
                        .show()
                }

            } else {
                //insert new data
                isEditMode = false
                btnAdd.text = "Thêm"
                btnDelete.visibility = View.GONE

                btnAdd.setOnClickListener {
                    val success: Long
                    val students = StudentModel()
                    if (isEditMode) {
                        //update
                        students.id = intent.getIntExtra("Id", 0)
                        students.name = edName.text.toString()
                        students.phone = edPhone.text.toString()
                        // Lấy giá trị năm sinh từ Spinner
                        val year = spinnerYear.selectedItem.toString()
                        students.birthDay = year
                        students.idStudent = edIdStudent.text.toString()
                        students.gender = gender
                        students.address = edAddress.text.toString()
                        students.semesterGrade1 = edSemesterGrade.text.toString().toDouble()
                        students.semesterGrade2 = edSemesterGrade2.text.toString().toDouble()
                        students.informationOther = informationOther.text.toString()
                        Log.d("Cập nhập dữ liệu", "studen.id = ${students.id} , ${students.name}")
                        //success = dbHandler.updateStudent(students)
                        Toast.makeText(this, "Đã cập nhật dữ liệu học sinh", Toast.LENGTH_SHORT)
                            .show()
                        val intent = Intent(this, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else {

                        students.name = edName.text.toString()
                        students.phone = edPhone.text.toString()
                        students.birthDay =
                            spinnerYear.selectedItem.toString() // Gán giá trị năm sinh từ Spinner
                        students.idStudent = edIdStudent.text.toString()
                        students.gender = gender
                        students.address = edAddress.text.toString()
                        students.semesterGrade1 = edSemesterGrade.text.toString().toDouble()
                        students.semesterGrade2 = edSemesterGrade2.text.toString().toDouble()
                        students.informationOther = informationOther.text.toString()
                        if (students.name.toString()
                                .isEmpty() || students.phone.isEmpty() || students.idStudent.isEmpty() || students.birthDay.isEmpty()
                        ) {
                            Toast.makeText(
                                this,
                                "Vui lòng nhập trường bắt buộc",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        } else {
                            success = dbHandler.addStudent(students)

                            if (success != -1L) {
                                Toast.makeText(this, "Đã thêm học sinh", Toast.LENGTH_SHORT).show()
                                edName.setText("")
                                edPhone.setText("")
                                edIdStudent.setText("")
                                edAddress.setText("")
                                edSemesterGrade.setText("")
                                edSemesterGrade2.setText("")
                                informationOther.setText("")
                                edName.requestFocus()

                                val intent = Intent(applicationContext, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(
                                    applicationContext,
                                    "Đã xảy ra sự cố!",
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                        }
                    }
                }
            }
        }
            btnDelete.setOnClickListener {
                Toast.makeText(applicationContext,"clicker",Toast.LENGTH_SHORT).show()
                //getStudent()
                val dialog =
                    AlertDialog.Builder(this).setTitle("Info")
                        .setMessage("Click nếu cậu muốn xoá nó!")
                        .setPositiveButton(
                            "Có nhé"
                        ) { _, _ ->
                            val success =
                                dbHandler.deleteStudent(intent.getIntExtra("Id", 0))
                            if (success) {
                                val intent = Intent(this, MainActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)

                                finish()
                            }
                        }
                        .setNegativeButton("Không muốn") { dialog, _ ->
                            dialog.dismiss()
                        }
                dialog.show()
            }

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        //val selectedYear = parent?.getItemAtPosition(position).toString()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
        //xu ly khi khong co lua chon nao duoc chon
    }

}
