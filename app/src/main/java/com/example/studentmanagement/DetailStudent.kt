package com.example.studentmanagement

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.example.studentmanagement.database.StudentDatabaseHelper
import com.example.studentmanagement.databinding.ActivityDetailStudentBinding
import com.example.studentmanagement.model.StudentModel

class DetailStudent : AppCompatActivity(),
    AdapterView.OnItemSelectedListener {
    private lateinit var binding: ActivityDetailStudentBinding
    override fun onCreate(savtxtInstanceState: Bundle?) {
        super.onCreate(savtxtInstanceState)
        setContentView(R.layout.activity_add_student_detail)

        binding = ActivityDetailStudentBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val txtName = binding.txtNamestd
        val txtPhone = binding.txtSdt
        val txtIdStudent = binding.txtMsv
        val txtBirthDay = binding.txtAge
        val txtGender = binding.txtGender
        val txtAddress = binding.txtAddress
        val txtSemesterGrade = binding.txtGrade1
        val txtSemesterGrade2 = binding.txtGrade2
        val txtGradeFinal = binding.txtDiemtk3
        val txtInfomationOther = binding.txtInfomatiobOther
        val viewGenderStudent = binding.imageviewStd
        val dbHandler = StudentDatabaseHelper(this)
        val students: StudentModel? = dbHandler.getStudent(intent.getIntExtra("Id", 0))
        val btnCallPhone = binding.btnCallPhone // Thay btnCallPhone bằng ID của nút
        val backButton = binding.saveSearchButton
        backButton.setOnClickListener {
            finish() // Sử dụng phương thức onBackPressed() để quay về trang trước đó
        }

        if (intent != null && intent.getStringExtra("Mode2") == "E2") {
            if (students != null) {
                txtName.text = students.name
                txtPhone.text = students.phone
                txtGender.text = students.gender
                if (txtGender.text == "Nam") {
                    viewGenderStudent.setImageResource(R.drawable.boy)
                } else if (txtGender.text == "Nữ") {
                    viewGenderStudent.setImageResource(R.drawable.woman)
                } else {
                    viewGenderStudent.setImageResource(androidx.appcompat.R.drawable.abc_text_select_handle_left_mtrl)
                }

                txtIdStudent.text = students.idStudent
                txtAddress.text = students.address
                txtSemesterGrade.text = students.semesterGrade1.toString()
                txtSemesterGrade2.text = students.semesterGrade2.toString()
                txtGradeFinal.text =
                    ((students.semesterGrade1 + students.semesterGrade2) / 2).toString()
                txtBirthDay.setText(students.birthDay).toString()
                txtInfomationOther.text = students.informationOther

            }
        }
        btnCallPhone.setOnClickListener {
            val phone = txtPhone.text.toString()

            // Tạo một Intent để gọi điện thoại
            val intent: Intent = Intent(Intent.ACTION_DIAL)//, Uri.parse("tel:$phone"))

            // Kiểm tra xem thiết bị có ứng dụng để xử lý Intent này không

            intent.data = Uri.parse("tel:$phone")
            startActivity(intent)

        }
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implementtxt")
    }

}
