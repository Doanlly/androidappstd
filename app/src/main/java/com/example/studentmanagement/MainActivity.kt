package com.example.studentmanagement

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import  androidx.recyclerview.widget.RecyclerView
import com.example.studentmanagement.database.StudentDatabaseHelper
import com.example.studentmanagement.model.StudentModel
import com.example.studentmanagement.adapter.StudentAdapter
import com.example.studentmanagement.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {

        //var adapter = StudentModel() //StudentAdapter(items)
        lateinit var studentListAdapter:StudentAdapter
        lateinit var studentList:List<StudentModel>
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val recyclerView: RecyclerView = findViewById(R.id.recycle_listStudentView)
        val linearLayoutManager = LinearLayoutManager(applicationContext)
        val dbHandler = StudentDatabaseHelper(this)
        var isListLoaded = false


        studentList = dbHandler.getAllStudent()
        studentListAdapter  = StudentAdapter(studentList,applicationContext)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = studentListAdapter
        studentListAdapter.notifyDataSetChanged()
        //fetchList()

        val searchEditText = findViewById<EditText>(R.id.search_id)
        val searchButton = findViewById<ImageView>(R.id.search_img_view)

        searchButton.setOnClickListener {
            val searchText = searchEditText.text.toString().trim()

            if (searchText.isNotEmpty()) {
                // Tìm kiếm và cập nhật dữ liệu dựa trên tên
                val filteredList = studentList.filter { it.name!!.contains(searchText, ignoreCase = true) }
                studentListAdapter.updateData(filteredList)
            } else {
                // Nếu trường tìm kiếm rỗng, hiển thị toàn bộ danh sách
                studentListAdapter.updateData(studentList)
            }
        }

        binding.floatingacBtnAddstd.setOnClickListener {
            //Toast.makeText(this,"clicked", Toast.LENGTH_SHORT).show()
            val intent = Intent(applicationContext, AddStudentDetail::class.java)
            startActivity(intent)
            finish()
        }
    }


}


////gridview
//recyclerView.layoutManager = GridLayoutManager(
//this,
//2,
//GridLayoutManager.VERTICAL//dieu huong
//,false//ko dao  nguoc
//)
//
//adapter.onItemClick = { clickedStudent ->
//    val intent = Intent(this, DetailStudent::class.java)
//    intent.putExtra("student", clickedStudent)
//    startActivity(intent)
//}
//        recyclerView.adapter = StudentAdapter(items)
//        recyclerView.layoutManager = LinearLayoutManager(
//            this,
//
//            GridLayoutManager.VERTICAL,
//            //LinearLayoutManager.HORIZONTAL,
//            false)
//    }
//        adapter.onItemClick = {
//            val intent = Intent(this,DetailStudent::class.java )
//            intent.putExtra("student", it)
//            startActivity(intent)
//        }
//        adapter.onItemClick = { student ->
//            val intent = Intent(this, DetailStudent::class.java)
//            intent.putExtra("student", student)
//            startActivity(intent)
//        }