package com.example.studentmanagement.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.studentmanagement.AddStudentDetail
import com.example.studentmanagement.DetailStudent
import com.example.studentmanagement.R
import com.example.studentmanagement.databinding.ItemstudentViewBinding
import com.example.studentmanagement.model.StudentModel

//Kotlin đã hỗ trợ khởi tạo thuộc tính trong constructor thông qua cú pháp ngắn gọn. To duoi tạo constructor chấp nhận context và items
class StudentAdapter(private var items: List<StudentModel>, private var context: Context) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {

    inner class StudentViewHolder( binding: ItemstudentViewBinding) : RecyclerView.ViewHolder(binding.root) {
        var nameStudent = binding.nameStdTt
        var phoneStudent = binding.phonestdTt
        val genderStudent = binding.imageviewStd
        var editStudent  = binding.btnEditStudent
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val binding = ItemstudentViewBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return StudentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }
    // Rest of the adapter code
    fun updateData(newList: List<StudentModel>) {
        items = newList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        val currentItem = items[position]

        holder.nameStudent.text = currentItem.name
        holder.phoneStudent.text = currentItem.phone
        if (currentItem.gender == "Nam"){
            holder.genderStudent.setImageResource(R.drawable.boy)
        }else{
            holder.genderStudent.setImageResource(R.drawable.woman)
        }
        holder.itemView.setOnClickListener {
            Toast.makeText(context, "Clicked ${holder.nameStudent.text}", Toast.LENGTH_SHORT).show()
            val i = Intent(context, DetailStudent::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK  // Add this line to set the flag
            i.putExtra("Mode2", "E2")
            i.putExtra("Id", currentItem.id)
            context.startActivity(i)
        }
        holder.editStudent.setOnClickListener {
            val i = Intent(context, AddStudentDetail::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK  // Add this line to set the flag
            i.putExtra("Mode", "E")
            i.putExtra("Id", currentItem.id)
            context.startActivity(i)
        }
    }
}

    //onbindViewHolder
    // Sử dụng 'context' và 'items' ở đây khi cần
    // Ví dụ: val currentItem = items[position]
    //        holder.textView.setText(items.get(position).Name)
    //        holder.textViewPhone.setText(items.get(position).Phone)
    //        Log.d("StudentAdapter", "Binding data for position $position")
    //        Log.d("StudentAdapter", "Name: ${items[position].Name}")
    //        Log.d("StudentAdapter", "Phone: ${items[position].Phone}")
    //        holder.textView.text = items[position].Name
    //                holder.textViewPhone.text = items[position].Phone.toString() // Chuyển đổi thành chuỗi vì Phone là kiểu Int
    //        holder.textView.setText(items.get(position).Name)
    //        holder.textViewPhone.setText(items.get(position).Phone.toString())
    //        holder.itemView.apply {
    //            name_std_tt = items[position].name
    //            contextphone_tt.text = items[position].phone.toString()
    //        }
