package com.example.studentmanagement.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.studentmanagement.model.StudentModel
import java.lang.Exception

class StudentDatabaseHelper(context: Context) : SQLiteOpenHelper(
    context,
    DATABASE_NAME, null,
    DATABASE_VERSION
) {

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "student.db"
        private const val TBL_STUDENT = "tbl_student"
        private const val ID = "id"
        private const val NAME = "name"
        private const val PHONE = "phone"
        private const val ID_STUDENT = "id_student"
        private const val BIRTHDAY = "birthday"
        private const val GENDER = "gender"
        private const val ADDRESS = "address"
        private const val SEMESTER_GRADE1 = "semester_grade1"
        private const val SEMESTER_GRADE2 = "semester_grade2"
        private const val INFORMATION_OTHER = "information_other"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTblStudent =
            ("CREATE TABLE $TBL_STUDENT (" +
                    "$ID INTEGER PRIMARY KEY," +
                    " $NAME TEXT," +
                    " $GENDER TEXT," +
                    " $PHONE TEXT," +
                    " $ID_STUDENT TEXT," +
                    " $BIRTHDAY TEXT," +
                    " $ADDRESS TEXT," +
                    " $SEMESTER_GRADE1 REAL," +
                    " $SEMESTER_GRADE2 REAL," +
                    " $INFORMATION_OTHER TEXT)")
        db?.execSQL(createTblStudent)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TBL_STUDENT")
        onCreate(db)
    }

    @SuppressLint("Range")
    //Lay thong tin cua cac hpc sinh tu 1 db va tra ve list cac obj StudentModel
    fun getAllStudent(): List<StudentModel> {

        //Thiet lap 1 list luu tru thong tin sinh vien
        val studentList = ArrayList<StudentModel>()
        val db: SQLiteDatabase = writableDatabase  // co the thuc hien cac truy van

        val selectQuery = "SELECT*FROM $TBL_STUDENT"

        //tao 1 obj cursor de thuc hien truy van do so du loeui
        val cursor: Cursor
        try {
            cursor = db.rawQuery(selectQuery, null)
            if (cursor != null) {
                if (cursor.moveToFirst()) { //check xem con tro co tro den dong 1
                    do {
                        val students = StudentModel()
                        students.id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ID)))
                        students.name = cursor.getString(cursor.getColumnIndex(NAME))
                        students.phone = cursor.getString(cursor.getColumnIndex(PHONE))
                        students.idStudent = cursor.getString(cursor.getColumnIndex(ID_STUDENT))
                        students.birthDay = cursor.getString(cursor.getColumnIndex(BIRTHDAY))
                        students.gender = cursor.getString(cursor.getColumnIndex(GENDER))
                        students.address = cursor.getString(cursor.getColumnIndex(ADDRESS))
                        students.semesterGrade1 = cursor.getDouble(cursor.getColumnIndex(
                            SEMESTER_GRADE1))
                        students.semesterGrade2 = cursor.getDouble(cursor.getColumnIndex(SEMESTER_GRADE2))
                        students.informationOther = cursor.getString(cursor.getColumnIndex(INFORMATION_OTHER))

                        studentList.add(students)
                    } while (cursor.moveToNext()) //tiep tuc lap den het
                }
            }
            cursor.close()
        } catch (e: Exception) {
            e.printStackTrace()
            db.execSQL(selectQuery)
            return ArrayList() //tra ve rong khi xay ra loi
        }

        return studentList
    }

    fun addStudent(student: StudentModel): Long { //stdmd:objchứa thông tin về sinh viên.
        val db = this.writableDatabase //khởi tạo để thao tác với cơ sở dữ liệu.

        val contentValue = ContentValues()//lưu trữ các giá trị của sinh viên.
        //contentValue.put(ID,stdmd.id)
        contentValue.put(NAME, student.name.toString())
        contentValue.put(PHONE, student.phone)
        contentValue.put(ID_STUDENT, student.idStudent)
        contentValue.put(BIRTHDAY, student.birthDay)
        contentValue.put(GENDER, student.gender)
        contentValue.put(ADDRESS, student.address)
        contentValue.put(SEMESTER_GRADE1, student.semesterGrade1)
        contentValue.put(SEMESTER_GRADE2 ,student.semesterGrade2)
        contentValue.put(INFORMATION_OTHER, student.informationOther)

        //chèn thông tin sinh viên vào bảng "TBL_STUDENT" trong cơ sở dữ liệu.
        val success: Long = db.insert(TBL_STUDENT, null, contentValue) //
        db.close()

        return success
    }

    fun getStudent(id: Int): StudentModel? {
        val db: SQLiteDatabase = readableDatabase
        val selectQuery = "SELECT * FROM $TBL_STUDENT WHERE id = $id"
        var students: StudentModel? = null

        val cursor: Cursor
        try {
            cursor = db.rawQuery(selectQuery, null)
            if (cursor != null && cursor.moveToFirst()) {
                students = StudentModel()
                val curId = cursor.getColumnIndex(ID)
                students.id = Integer.parseInt(cursor.getString(curId))
                val curName = cursor.getColumnIndex(NAME)
                students.name = cursor.getString(curName)
                val curPhone = cursor.getColumnIndex(PHONE)
                students.phone = cursor.getString(curPhone)
                val curIdStudent = cursor.getColumnIndex(ID_STUDENT)
                students.idStudent = cursor.getString(curIdStudent)
                val curBirthDay = cursor.getColumnIndex(BIRTHDAY)
                students.birthDay = cursor.getString(curBirthDay)
                val curGender = cursor.getColumnIndex(GENDER)
                students.gender = cursor.getString(curGender)

                val curAddress = cursor.getColumnIndex(ADDRESS)
                students.address = cursor.getString(curAddress)
                val curSEMESTERGRADE1 = cursor.getColumnIndex(SEMESTER_GRADE1)
                students.semesterGrade1 = cursor.getDouble(curSEMESTERGRADE1)
                val curSEMESTERGRADE2 = cursor.getColumnIndex(SEMESTER_GRADE2)
                students.semesterGrade2 = cursor.getDouble(curSEMESTERGRADE2)
                val curInfomation = cursor.getColumnIndex(INFORMATION_OTHER)
                students.informationOther = cursor.getString(curInfomation)
            }
            cursor?.close()
        } catch (e: Exception) {
            e.printStackTrace()
            db.execSQL(selectQuery)
        }

        return students
    }

    fun deleteStudent(id: Int): Boolean {
        val db: SQLiteDatabase =
            this.writableDatabase //truy capj dden dung instance cua lop hien tai

        val success: Long = db.delete(TBL_STUDENT, "$ID=?", arrayOf(id.toString())).toLong()
        db.close()
        return Integer.parseInt("$success") != -1

    }

    fun updateStudent(students: StudentModel): Boolean{
        val db: SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(NAME, students.name)
        values.put(PHONE, students.phone)
        values.put(ID_STUDENT, students.idStudent)
        values.put(BIRTHDAY, students.birthDay)
        values.put(GENDER, students.gender)
        values.put(ADDRESS, students.address)
        values.put(SEMESTER_GRADE1, students.semesterGrade1)
        values.put(SEMESTER_GRADE2, students.semesterGrade2)
        values.put(INFORMATION_OTHER,students.informationOther)

        Log.d("Update Student", "studen.id = ${students.id}")
        //        val success: Long =
        //            db.update(TBL_STUDENT, values, "$ID=?", arrayOf(students.id.toString())).toLong()
        //        db.close()
        val whereClause = "$ID = ?"
        val whereArgs = arrayOf(students.id.toString())

        // Thực hiện cập nhật và kiểm tra kết quả
        val numberOfRowsUpdated = db.update(TBL_STUDENT, values, whereClause, whereArgs)
        db.close()
        return numberOfRowsUpdated > 0
       // return success//Integer.parseInt("$sucess")!= 1
    }

}





//    fun insertStudent(stdmd: StudentModel):Long{
//        val db = this.writableDatabase
//
//        val contentValue = ContentValues()
//        contentValue.put(ID,stdmd.id)
//        contentValue.put(NAME,stdmd.name)
//        contentValue.put(PHONE,stdmd.phone)
//
//        val sucess = db.insert(TBL_STUDENT,null,contentValue)
//        db.close()
//        return sucess
//    }
//chen du lieu vo tbl
//    fun addStudent(students:StudentModel):Boolean{
//        val db:SQLiteDatabase = this.writableDatabase //dao obj  de ghi data vap csdl
//        val values = ContentValues() //tao 1 obj co value
//        //gan thong tin vao
//        values.put(NAME,students.name)
//        values.put(PHONE,students.phone)
//        val success:Long = db.insert(NAME,null,values)
//        db.close()
//        return (Integer.parseInt("$success")!= -1)
//    }

