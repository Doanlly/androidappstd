package com.example.studentmanagement.model


class StudentModel {
    var id: Int = 0
    var idStudent:String = ""
    var name: String? = ""
    var phone: String = ""
    var birthDay:String = ""
    var gender:String= ""
    var address:String = ""
    var semesterGrade1:Double = 0.0
    var semesterGrade2:Double = 0.0
    var informationOther = ""
}
